json.extract! listing, :id, :buysell, :product, :price, :contacts, :description, :password, :created_at, :updated_at
json.url listing_url(listing, format: :json)
